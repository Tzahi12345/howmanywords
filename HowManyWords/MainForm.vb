﻿Public Class MainForm

    'Defines the variables needed later on (corresponds with textboxes)
    Dim ResolutionX As Decimal
    Dim ResolutionY As Decimal
    Dim AspectRatioX As Decimal
    Dim AspectRatioY As Decimal
    Dim WordsPerPicture As Decimal

    'Makes sure that only numbers and decimal points can be inputted into the texboxes
    Private Sub ResolutionXTextbox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ResolutionXTextbox.KeyPress,
        ResolutionYTextbox.KeyPress, AspectRatioXTextbox.KeyPress, AspectRatioYTextbox.KeyPress, WordsPerPictureTextbox.KeyPress
        If Not Char.IsDigit(e.KeyChar) And Not Char.IsControl(e.KeyChar) And Not e.KeyChar = "." Then
            e.Handled = True
        End If
    End Sub

    'All these functions run when the textboxes change
    'It updates each variable with its respective textboxes

    Private Sub ResolutionXTextbox_TextChanged(sender As Object, e As EventArgs) Handles ResolutionXTextbox.TextChanged
        If ResolutionXTextbox.Text IsNot "" Then
            Try
                If ResolutionXTextbox.Text IsNot "" Then
                    ResolutionX = Convert.ToDecimal(ResolutionXTextbox.Text)
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub ResolutionYTextbox_TextChanged(sender As Object, e As EventArgs) Handles ResolutionYTextbox.TextChanged
        If ResolutionYTextbox.Text IsNot "" Then
            Try
                If ResolutionYTextbox.Text IsNot "" Then
                    ResolutionY = Convert.ToDecimal(ResolutionYTextbox.Text)
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub AspectRatioXTextbox_TextChanged(sender As Object, e As EventArgs) Handles AspectRatioXTextbox.TextChanged
        If AspectRatioXTextbox.Text IsNot "" Then
            Try
                If AspectRatioXTextbox.Text IsNot "" Then
                    AspectRatioX = Convert.ToDecimal(AspectRatioXTextbox.Text)
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub AspectRatioYTextbox_TextChanged(sender As Object, e As EventArgs) Handles AspectRatioYTextbox.TextChanged
        If AspectRatioYTextbox.Text IsNot "" Then
            Try
                If AspectRatioYTextbox.Text IsNot "" Then
                    AspectRatioY = Convert.ToDecimal(AspectRatioYTextbox.Text)
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub WordsPerPictureTextbox_TextChanged(sender As Object, e As EventArgs) Handles WordsPerPictureTextbox.TextChanged
        If WordsPerPictureTextbox.Text IsNot "" Then
            Try
                If WordsPerPictureTextbox.Text IsNot "" Then
                    WordsPerPicture = Convert.ToDecimal(WordsPerPictureTextbox.Text)
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    'Does the math for both textboxes

    Private Sub FirstCalculateButton_Click(sender As Object, e As EventArgs) Handles FirstCalculateButton.Click
        'Makes sure the textboxes are not empty, unfortunately necessary
        If ResolutionXTextbox.Text IsNot "" And ResolutionYTextbox.Text IsNot "" Then
            Dim XTimesY As Decimal = ResolutionX * ResolutionY
            Dim Words As Decimal = Math.Round(0.00295 * XTimesY, 1)
            FirstResultLabel.Text = Words & " words per picture"
        Else
            MsgBox("Please input valid numbers for both resolution textboxes!")
        End If
    End Sub

    Private Sub SecondCalculateButton_Click(sender As Object, e As EventArgs) Handles SecondCalculateButton.Click
        'Makes sure the textboxes are not empty and additionally checks that the AspectRatioY is not 0 to avoid dividing by 0
        If AspectRatioXTextbox.Text IsNot "" And AspectRatioYTextbox.Text IsNot "" And AspectRatioY <> 0 And WordsPerPictureTextbox.Text IsNot "" Then
            Dim Coefficient As Decimal = AspectRatioX / AspectRatioY
            Dim TotalPixels As Decimal = WordsPerPicture / 0.00295
            Dim ResultBeforeSqrt As Decimal = TotalPixels / Coefficient
            Dim ResultY As Decimal = Math.Sqrt(ResultBeforeSqrt)
            Dim ResultYRound As Decimal = Math.Round(ResultY, 0)
            Dim ResultX As Decimal = ResultY * Coefficient
            Dim ResultXRound As Decimal = Math.Round(ResultX, 0)
            SecondResultLabel.Text = ResultXRound & " by " & ResultYRound
        Else
            MsgBox("Please input valid numbers for all textboxes!")
        End If
    End Sub

    'Does some prelimenary stuff as the program starts
    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Makes the result labels empty
        FirstResultLabel.Text = ""
        SecondResultLabel.Text = ""
        'Sets the first calculate button as the "enter" button
        Me.MainTab.TabIndex = 0
        Me.AcceptButton = Me.FirstCalculateButton
    End Sub

    'Switches which button is the "enter" button, depending on which tab is pressed. So on the second tab, pressing enter will calculate the numbers on that tab
    Private Sub MainTab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MainTab.SelectedIndexChanged
        Select Case Me.MainTab.SelectedIndex
            Case Is = 0
                Me.AcceptButton = Me.FirstCalculateButton
            Case Is = 1
                Me.AcceptButton = Me.SecondCalculateButton
        End Select
    End Sub

End Class

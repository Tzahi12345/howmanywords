﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MainTab = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.FirstResultLabel = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.FirstCalculateButton = New System.Windows.Forms.Button()
        Me.ResolutionYTextbox = New System.Windows.Forms.TextBox()
        Me.ResolutionXTextbox = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.WordsPerPictureTextbox = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.SecondResultLabel = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.SecondCalculateButton = New System.Windows.Forms.Button()
        Me.AspectRatioYTextbox = New System.Windows.Forms.TextBox()
        Me.AspectRatioXTextbox = New System.Windows.Forms.TextBox()
        Me.MainTab.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainTab
        '
        Me.MainTab.Controls.Add(Me.TabPage1)
        Me.MainTab.Controls.Add(Me.TabPage2)
        Me.MainTab.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainTab.Location = New System.Drawing.Point(0, 0)
        Me.MainTab.Name = "MainTab"
        Me.MainTab.SelectedIndex = 0
        Me.MainTab.Size = New System.Drawing.Size(660, 323)
        Me.MainTab.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.FirstResultLabel)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.FirstCalculateButton)
        Me.TabPage1.Controls.Add(Me.ResolutionYTextbox)
        Me.TabPage1.Controls.Add(Me.ResolutionXTextbox)
        Me.TabPage1.Location = New System.Drawing.Point(8, 39)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(644, 276)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "WPP Calculator"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'FirstResultLabel
        '
        Me.FirstResultLabel.AutoSize = True
        Me.FirstResultLabel.Location = New System.Drawing.Point(113, 153)
        Me.FirstResultLabel.Name = "FirstResultLabel"
        Me.FirstResultLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FirstResultLabel.Size = New System.Drawing.Size(173, 25)
        Me.FirstResultLabel.TabIndex = 14
        Me.FirstResultLabel.Text = "RESULT_LABEL"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(28, 153)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(79, 25)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Result:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(341, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 25)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "by"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 25)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Resolution:"
        '
        'FirstCalculateButton
        '
        Me.FirstCalculateButton.Location = New System.Drawing.Point(24, 73)
        Me.FirstCalculateButton.Name = "FirstCalculateButton"
        Me.FirstCalculateButton.Size = New System.Drawing.Size(121, 48)
        Me.FirstCalculateButton.TabIndex = 3
        Me.FirstCalculateButton.Text = "Calculate"
        Me.FirstCalculateButton.UseVisualStyleBackColor = True
        '
        'ResolutionYTextbox
        '
        Me.ResolutionYTextbox.Location = New System.Drawing.Point(386, 13)
        Me.ResolutionYTextbox.Name = "ResolutionYTextbox"
        Me.ResolutionYTextbox.Size = New System.Drawing.Size(175, 31)
        Me.ResolutionYTextbox.TabIndex = 2
        '
        'ResolutionXTextbox
        '
        Me.ResolutionXTextbox.Location = New System.Drawing.Point(156, 13)
        Me.ResolutionXTextbox.Name = "ResolutionXTextbox"
        Me.ResolutionXTextbox.Size = New System.Drawing.Size(175, 31)
        Me.ResolutionXTextbox.TabIndex = 1
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.WordsPerPictureTextbox)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.SecondResultLabel)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.SecondCalculateButton)
        Me.TabPage2.Controls.Add(Me.AspectRatioYTextbox)
        Me.TabPage2.Controls.Add(Me.AspectRatioXTextbox)
        Me.TabPage2.Location = New System.Drawing.Point(8, 39)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(755, 395)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Resolution Calculator"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'WordsPerPictureTextbox
        '
        Me.WordsPerPictureTextbox.Location = New System.Drawing.Point(227, 64)
        Me.WordsPerPictureTextbox.Name = "WordsPerPictureTextbox"
        Me.WordsPerPictureTextbox.Size = New System.Drawing.Size(108, 31)
        Me.WordsPerPictureTextbox.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(19, 67)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(190, 25)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Words per Picture:"
        '
        'SecondResultLabel
        '
        Me.SecondResultLabel.AutoSize = True
        Me.SecondResultLabel.Location = New System.Drawing.Point(113, 204)
        Me.SecondResultLabel.Name = "SecondResultLabel"
        Me.SecondResultLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SecondResultLabel.Size = New System.Drawing.Size(173, 25)
        Me.SecondResultLabel.TabIndex = 21
        Me.SecondResultLabel.Text = "RESULT_LABEL"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(28, 204)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(79, 25)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "Result:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(279, 16)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(35, 25)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "by"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(19, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(140, 25)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "Aspect Ratio:"
        '
        'SecondCalculateButton
        '
        Me.SecondCalculateButton.Location = New System.Drawing.Point(24, 124)
        Me.SecondCalculateButton.Name = "SecondCalculateButton"
        Me.SecondCalculateButton.Size = New System.Drawing.Size(121, 48)
        Me.SecondCalculateButton.TabIndex = 7
        Me.SecondCalculateButton.Text = "Calculate"
        Me.SecondCalculateButton.UseVisualStyleBackColor = True
        '
        'AspectRatioYTextbox
        '
        Me.AspectRatioYTextbox.Location = New System.Drawing.Point(320, 13)
        Me.AspectRatioYTextbox.Name = "AspectRatioYTextbox"
        Me.AspectRatioYTextbox.Size = New System.Drawing.Size(100, 31)
        Me.AspectRatioYTextbox.TabIndex = 5
        '
        'AspectRatioXTextbox
        '
        Me.AspectRatioXTextbox.Location = New System.Drawing.Point(165, 13)
        Me.AspectRatioXTextbox.Name = "AspectRatioXTextbox"
        Me.AspectRatioXTextbox.Size = New System.Drawing.Size(108, 31)
        Me.AspectRatioXTextbox.TabIndex = 4
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(660, 323)
        Me.Controls.Add(Me.MainTab)
        Me.Name = "MainForm"
        Me.Text = "How Many Words?"
        Me.MainTab.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents MainTab As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents FirstResultLabel As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents FirstCalculateButton As Button
    Friend WithEvents ResolutionYTextbox As TextBox
    Friend WithEvents ResolutionXTextbox As TextBox
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents WordsPerPictureTextbox As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents SecondResultLabel As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents SecondCalculateButton As Button
    Friend WithEvents AspectRatioYTextbox As TextBox
    Friend WithEvents AspectRatioXTextbox As TextBox
End Class
